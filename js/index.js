const RED_ICON = L.icon({
    iconUrl: './libs/leaflet/images/redmarker-icon.png',
    iconRetinaUrl: './libs/leaflet/images/redmarker-icon-2x.png',
    shadowUrl: './libs/leaflet/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});
let index = {};
let map = L.map('map').setView([-34.5221554, -58.7000067], 12);
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
let markers = [];
let redMarker = {};
let redBorder = {};
let activitiesCounter = 1;

$('document').ready(() => {
    index = fillFullGrid(artworks);
    buildGalleryFilters(index);
    buildGallery(index);

    $('#searchForm').submit(event => {
        event.preventDefault();
        if (event.target.criteria.value.length > 1) {
            clearCriteria('search');
            clearCriteria('filter');
            if ($('#searchRadios1').is(':checked')) {
                $('#criteriaDiv').append(`<p id="searchCriteria" class="h4 font-weight-light mb-3 mr-3">Búsqueda: ${event.target.criteria.value}</p>`);
                index = filter(fillFullGrid(artworks), event.target.criteria.value);
                buildGalleryFilters(index);
                buildGallery(index);
            } else if ($('#searchRadios2').is(':checked')) {
                $('#criteriaDiv').append(`<p id="searchCriteria" class="h4 font-weight-light mb-3 mr-3">Búsqueda: ${event.target.criteria.value}</p>`);
                index = filter(fillFullGrid(artStudios), event.target.criteria.value);
                buildStudiosFilters(index);
                buildArtStudiosGrid(index);
            }
        } else {
            toggleGenericModal('¡Búsqueda inválida!', 'bad-search.png', '50', 'Escribí en el buscador lo que querés encontrar <b>(las búsquedas deben contener más de una letra)</b>.');
        }
    });

    $('#galleryBtn').click(() => {
        clearCriteria('search');
        clearCriteria('filter');
        index = fillFullGrid(artworks);
        buildGalleryFilters(index);
        buildGallery(index);
    });

    $('#artStudiosBtn').click(() => {
        clearCriteria('search');
        clearCriteria('filter');
        index = fillFullGrid(artStudios);
        buildStudiosFilters(index);
        buildArtStudiosGrid(index);
    });

    $('#registryBtn').click(() => {
        $('#registryModal').modal('toggle');
    });

    $('#inputFile').change(event => {
        $('#inputFileLabel').html(event.target.value);
    });

    $('#normalizeBtn').click(() => {
        let value = $('#inputAddress')[0].value;
        if (value != '') {
            $.ajax({
                method: "GET",
                url: `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${value}`,
            }).done(data => {
                $('#normalizationError').html('');
                if (data.direccionesNormalizadas.length > 0) {
                    $('#inputNormalizedAddress').removeAttr('readonly');
                    $('#inputNormalizedAddress').html('<option selected value="">Seleccione la dirección correspondiente...</option>');
                    for (let i = 0; i < data.direccionesNormalizadas.length; i++) {
                        let actualAddress = data.direccionesNormalizadas[i];
                        let addressString = `${actualAddress.direccion} (${actualAddress.nombre_localidad})`;
                        $('#inputNormalizedAddress').append(`<option value="${addressString}">${addressString}</option>`);
                    }
                } else {
                    $('#inputNormalizedAddress').attr('readonly', 'readonly');
                    $('#normalizationError').html(`${data.errorMessage}`);
                    $('#inputNormalizedAddress').html('<option selected value="">Ingrese la dirección en el campo Dirección</option>');
                }
            }).fail(error => {
                console.log(error);
            });
        }
    });

    $('#addBtn').click(() => {
        activitiesCounter++;
        $('#activitiesDiv').append(`
            <div class="border border-dark rounded-lg mt-1">
                <p class="mb-n1 ml-2 mt-2"><b>Actividad ${activitiesCounter}</b></p>
                <div class="form-row ml-2 mr-2">
                    <div class="form-group col-md-6">
                        <label for="inputActivityName${activitiesCounter}">Nombre de la actividad</label>
                        <input class="form-control" name="activityName${activitiesCounter}" id="inputActivityName${activitiesCounter}" placeholder="Nombre">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputSchedule${activitiesCounter}">Horarios</label>
                        <input class="form-control" name="schedule${activitiesCounter}" id="inputSchedule${activitiesCounter}" placeholder="Horarios">
                    </div>
                </div>
                <div class="form-row ml-2 mr-2">
                    <div class="form-group col-md-12">
                        <label for="inputActivityDescription${activitiesCounter}">Descripción</label>
                        <textarea class="form-control" name="activityDescription${activitiesCounter}" id="inputActivityDescription${activitiesCounter}" rows="2" placeholder="Descripción..."></textarea>
                    </div>
                </div>
                <div class="form-row ml-2 mr-2">
                    <div class="form-group col-md-6">
                        <div class="form-group">
                        <label for="inputActivityFile${activitiesCounter}">Imágen</label>
                        <input type="file" class="form-control-file" id="inputActivityFile${activitiesCounter}" name="activityFile${activitiesCounter}">
                        </div>
                    </div>
                </div>
            </div>`);
        $(`#inputActivityName${activitiesCounter}`).rules('add', {
            required: true,
            messages: {
                required: 'Este campo es obligatorio'
            }
        });
        $(`#inputSchedule${activitiesCounter}`).rules('add', {
            required: true,
            messages: {
                required: 'Este campo es obligatorio'
            }
        });
        $(`#inputActivityDescription${activitiesCounter}`).rules('add', {
            required: true,
            messages: {
                required: 'Este campo es obligatorio'
            }
        });
        $(`#inputActivityFile${activitiesCounter}`).rules('add', {
            required: true,
            messages: {
                required: 'Este campo es obligatorio'
            }
        });
    });

    $('#registryForm').validate({
        rules: {
            name: "required",
            file: "required",
            email: { email: true },
            phone: "required",
            description: "required",
            normalizedAddress: "required",
            activityName: "required",
            schedule: "required",
            activityDescription: "required",
            activityFile: "required"
        },
        messages: {
            name: {
                required: 'Este campo es obligatorio'
            },
            file: {
                required: 'Debe seleccionar una imágen para su taller'
            },
            email: {
                email: 'Ingrese un mail válido'
            },
            phone: {
                required: 'Este campo es obligatorio'
            },
            description: {
                required: 'Este campo es obligatorio'
            },
            normalizedAddress: {
                required: 'Debe seleccionar una dirección'
            },
            activityName: {
                required: 'Este campo es obligatorio'
            },
            schedule: {
                required: 'Este campo es obligatorio'
            },
            activityDescription: {
                required: 'Este campo es obligatorio'
            },
            activityFile: {
                required: 'Este campo es obligatorio'
            }
        },
        submitHandler: () => {
            $('#registryModal').modal('toggle');
            toggleGenericModal('Registro enviado', 'success.png', '50', '<b>¡Gracias por registrar su taller!</b><br><br>Tu solicitud está a la espera de ser aprobada. Te avisaremos cuando tu taller esté disponible.');
            $('#registryBtn').hide();
        },
        errorClass: "errorTxt"
    });

    $('#closeRegisterBtn').click(() => {
        $('#registryModal').modal('toggle');
        $('#registryForm').trigger("reset");
        $('#registryForm').validate().resetForm();
        $('#normalizationError').html('');
        $('#activitiesDiv').html(`<p class="mb-n3">Actividades<br><br></p>
            <div class="border border-dark rounded-lg">
                <p class="mb-n1 ml-2 mt-2"><b>Actividad 1</b></p>
                <div class="form-row ml-2 mr-2">
                    <div class="form-group col-md-6">
                        <label for="inputActivityName">Nombre de la actividad</label>
                        <input class="form-control" name="activityName" id="inputActivityName" placeholder="Nombre">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="inputSchedule">Horarios</label>
                        <input class="form-control" name="schedule" id="inputSchedule" placeholder="Horarios">
                    </div>
                </div>
                <div class="form-row ml-2 mr-2">
                    <div class="form-group col-md-12">
                        <label for="inputActivityDescription">Descripción</label>
                        <textarea class="form-control" name="activityDescription" id="inputActivityDescription" rows="2" placeholder="Descripción..."></textarea>
                    </div>
                </div>
                <div class="form-row ml-2 mr-2">
                    <div class="form-group col-md-6">
                        <div class="form-group">
                            <label for="inputActivityFile">Imágen</label>
                            <input type="file" class="form-control-file" id="inputActivityFile" name="activityFile">
                        </div>
                    </div>
                </div>
            </div>`)
    });

    $('#usBtn').click(event => {
        event.preventDefault();
        toggleGenericModal('Acerca de nosotros', 'logo.png', '200', `<b>ProyectARTE</b> es un sitio destinado a la ilustración y representación de distintos movimientos artísticos. Nuestro objetivo es brindarte los talleres y obras que más se adapten a tus necesidades.
        <br><br>¡Te invitamos a navegar por nuestra galería virtual con numerosas publicaciones y a que encuentres tu taller de arte preferido!`);
    });

    $('#helpBtn').click(event => {
        event.preventDefault();
        toggleGenericModal('¿En qué podemos ayudarte?', 'help.png', '50', `Nuestro soporte en tiempo real aún no esta disponible, te pedimos disculpas.
        <br><br><b>¡Pero pará!</b> Clickeando en el botón "<b>Contáctanos</b>" ubicado al pie del sitio podés escribirnos tu consulta o problema y te responderemos en breve.
        <br><br><b>¡Muchas gracias!</b>`);
    });

    $('#contactBtn').click(event => {
        event.preventDefault();
        $('#contactModal').modal('toggle');
    });

    $('#closeBtn').click(() => {
        $('#contactModal').modal('toggle');
        $('#contactForm').trigger("reset");
        $('#contactForm').validate().resetForm();
    });

    $('#contactForm').validate({
        rules: {
            names: "required",
            surnames: "required",
            email: {
                required: true,
                email: true
            },
            query: "required"
        },
        messages: {
            names: {
                required: 'El campo Nombre/s es obligatorio'
            },
            surnames: {
                required: 'El campo Apellido/s es obligatorio'
            },
            email: {
                required: 'El campo Email es obligatorio',
                email: 'El mail ingresado es inválido.'
            },
            query: {
                required: '¡No te olvides de hacernos tu consulta!'
            }
        },
        submitHandler: () => {
            $('#contactModal').modal('toggle');
            toggleGenericModal('Consulta enviada', 'success.png', '50', '<b>¡Gracias por contactarnos!</b><br><br>Te enviaremos una respuesta a la dirección de mail indicada.');
            $('#contactForm').trigger("reset");
        },
        errorClass: "errorTxt"
    });

    $('#topBtn').click(() => {
        $('html, body').animate({ scrollTop: 0 }, 800);
    });
});