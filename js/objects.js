const Grid = function () {
    let items = [];

    this.addItem = function (item) {
        items.push(item);
    }

    this.getItems = () => items;

    this.shuffle = function () {
        for (let i = items.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [items[i], items[j]] = [items[j], items[i]];
        }
    }

    this.putSponsorsFirst = function () {
        let sponsors = [];
        let noSponsors = [];
        for (let i = 0; i < items.length; i++) {
            if (items[i].sponsor)
                sponsors.push(items[i]);
            else
                noSponsors.push(items[i]);
        }
        items = sponsors.concat(noSponsors);
    }
}