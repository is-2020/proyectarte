function fillFullGrid(array) {
    const grid = new Grid();
    array.forEach(item => {
        grid.addItem(item);
    });
    grid.shuffle();
    return grid;
}

function buildGalleryFilters(gallery) {
    let filters = ['▼ AÑO', '▼ AUTOR', '▼ TÉCNICA', '▼ ESTILO', '▼ CONDICIÓN', '▼ DISPONIBILIDAD DE VENTA'];
    let alreadyAdded = [];
    $('#filtersList').html('');
    for (let i = 0; i < 6; i++) {
        let aux = '';
        $('#filtersList').append(`<li><a href="#" class="text-body" id="categoryHeader${i}" style="text-decoration:none;">${filters[i]}</a></li>`);
        $(`#categoryHeader${i}`).append(`<ul class="m-1" id="category${i}"></ul>`);
        gallery.getItems().forEach(artwork => {
            switch (i) {
                case 0: aux = artwork.year; break;
                case 1: aux = artwork.author; break;
                case 2: aux = artwork.technique; break;
                case 3: aux = artwork.style; break;
                case 4: aux = artwork.condition; break;
                case 5:
                    if (artwork.onSale)
                        aux = "Disponible";
                    else
                        aux = "No disponible";
                    break;
            }
            if (!alreadyAdded.includes(aux)) {
                alreadyAdded.push(aux);
                if (aux === "Disponible") {
                    $(`#category${i}`).append(`
                        <li><a href="#" class="text-body categoryItem" data-value="Si disponible" style="text-decoration:none;">${aux}</a></li>`);
                } else {
                    $(`#category${i}`).append(`
                    <li><a href="#" class="text-body categoryItem" data-value="${aux}" style="text-decoration:none;">${aux}</a></li>`);
                }
            }
        });
        $(`#category${i}`).hide();
        $(`#categoryHeader${i}`).click(event => {
            event.preventDefault();
            if ($(`#category${i}`).is(':hidden')) {
                $(`#category${i}`).slideDown("slow");
            } else {
                $(`#category${i}`).slideUp('slow');
            }
        });
    }
    $('.categoryItem').click(event => {
        clearCriteria('filter');
        $('#criteriaDiv').append(`<p id="filterCriteria" class="h4 font-weight-light mb-3">Filtro: ${event.target.dataset.value}</p>`);
        let temp = filter(index, event.target.dataset.value);
        buildGallery(temp);
    });
}

function buildStudiosFilters(grid) {
    let filters = ['▼ ACTIVIDAD', '▼ MUNICIPIO', '▼ AÑO DE INSCRIPCIÓN'];
    let alreadyAdded = [];
    $('#filtersList').html('');
    for (let i = 0; i < 3; i++) {
        let aux = {};
        $('#filtersList').append(`<li><a href="#" class="text-body" id="categoryHeader${i}" style="text-decoration:none;">${filters[i]}</a></li>`);
        $(`#categoryHeader${i}`).append(`<ul class="m-1" id="category${i}"></ul>`);
        grid.getItems().forEach(studio => {
            switch (i) {
                case 0:
                    for (let j = 0; j < studio.activities.length; j++) {
                        aux = studio.activities[j].name;
                        if (!alreadyAdded.includes(aux)) {
                            alreadyAdded.push(aux);
                            $(`#category${i}`).append(`
                            <li><a href="#" class="text-body categoryItem" data-value="${aux}" style="text-decoration:none;">${aux}</a></li>`);
                        }
                    }
                    break;
                case 1: aux = studio.city; break;
                case 2: aux = studio.year; break;
            }
            if (i != 0 && !alreadyAdded.includes(aux)) {
                alreadyAdded.push(aux);
                $(`#category${i}`).append(`
                    <li><a href="#" class="text-body categoryItem" data-value="${aux}" style="text-decoration:none;">${aux}</a></li>`);
            }
        });
        $(`#category${i}`).hide();
        $(`#categoryHeader${i}`).click(event => {
            event.preventDefault();
            if ($(`#category${i}`).is(':hidden')) {
                $(`#category${i}`).slideDown("slow");
            } else {
                $(`#category${i}`).slideUp('slow');
            }
        });
    }
    $('.categoryItem').click(event => {
        clearCriteria('filter');
        $('#criteriaDiv').append(`<p id="filterCriteria" class="h4 font-weight-light mb-3">Filtro: ${event.target.dataset.value}</p>`);
        let temp = filter(index, event.target.dataset.value);
        buildArtStudiosGrid(temp);
    });
}

function buildGallery(gallery) {
    const $itemsRef = $('#items');
    $itemsRef.html('');
    let aCounter = 0;
    $('#mapDiv').hide();
    $('#vl2').hide();
    $('#grid-title').text('Galería virtual');
    for (let i = 0; i < gallery.getItems().length; i++) {
        const actualArtwork = gallery.getItems()[i];
        aCounter++;
        $itemsRef.append(`
            <div class="col-3">
                <button class="btn" type="button">
                    <img src=${actualArtwork.imagePath} id=${"img" + actualArtwork.id} class="img-fluid border border-dark rounded-lg w-100" style="height: auto;">
                </button>
                <h5 class="mt-1 mb-1">${actualArtwork.name}</h5>
                <p>${actualArtwork.author}</p>
            </div>`);
        $('#img' + actualArtwork.id).click(() => {
            fillItemView('artwork', findItem(actualArtwork.id, artworks));
            $('#visualizationModal').modal('toggle');
        });
    }
    if (aCounter === 0) {
        $itemsRef.append(`<div class="container text-center"><p style="font-size: medium;height:300px;"><b>No hay resultados para mostrar.</b></p></div>`);
    }
}

function buildArtStudiosGrid(grid) {
    const $itemsRef = $('#items');
    $itemsRef.html('');
    let asCounter = 0;
    removeAllMarkers();
    $('#mapDiv').show();
    $('#vl2').show();
    $('#grid-title').text('Talleres');
    grid.putSponsorsFirst();
    for (let i = 0; i < grid.getItems().length; i++) {
        const actualStudio = grid.getItems()[i];
        asCounter++;
        if (actualStudio.sponsor) {
            $itemsRef.append(`
            <div class="col-3">
                <button class="btn" type="button">
                    <img src=${actualStudio.imagePath} id=${"img" + actualStudio.id} class="img-fluid border border-dark rounded-lg w-100" style="height: auto;">
                </button>
                <h5 class="mt-1 mb-1">
                    <img src="./assets/star.png" class="mt-n1" style="height: 17px;"  data-toggle="tooltip" data-placement="top" title="Taller patrocinador"> ${actualStudio.name}
                </h5>
                <p>${actualStudio.address}</p>
                <button type="button" id=${"mark" + actualStudio.id} class="btn mt-n2 mb-3" style="background-color: rgb(233, 159, 79);" data-toggle="tooltip" data-placement="top" title="Destacar en el mapa">
                    <img src="assets/mark.png" style="height: 25px;">
                </button>
            </div>`);
        } else {
            $itemsRef.append(`
                <div class="col-3">
                    <button class="btn" type="button">
                        <img src=${actualStudio.imagePath} id=${"img" + actualStudio.id} class="img-fluid border border-dark rounded-lg w-100" style="height: auto;">
                    </button>
                    <h5 class="mt-1 mb-1">${actualStudio.name}</h5>
                    <p>${actualStudio.address}</p>
                    <button type="button" id=${"mark" + actualStudio.id} class="btn mt-n2 mb-3" style="background-color: rgb(233, 159, 79);" data-toggle="tooltip" data-placement="top" title="Destacar en el mapa">
                        <img src="assets/mark.png" style="height: 25px;">
                    </button>
                </div>`);
        }
        $('#img' + actualStudio.id).click(() => {
            fillItemView('artStudio', findItem(actualStudio.id, artStudios));
            $('#visualizationModal').modal('toggle');
        });
        $('#mark' + actualStudio.id).click(() => {
            addMarker(L.marker(actualStudio.coordinates, { icon: RED_ICON }), actualStudio, 'red');
            redMarker.openPopup();
            updateRedBorder(actualStudio);
        });
        addMarker(L.marker(actualStudio.coordinates), actualStudio);
    }
    if (asCounter === 0) {
        $('#mapDiv').hide();
        $('#vl2').hide();
        $itemsRef.append(`<div class="container text-center"><p style="font-size: medium;height:300px;"><b>No hay resultados para mostrar.</b></p></div>`);
    }
}

function findItem(id, array) {
    let ret = {};
    array.forEach(item => {
        if (item.id === id) {
            ret = item;
        }
    });
    return ret;
}

function fillItemView(type, item) {
    clearVisualizationData();
    $('#backBtn').hide();
    $('#name').text(item.name);
    $('#image').attr('src', () => {
        return item.imagePath;
    });
    $('#description').html(item.description);
    if (type === "artwork") {
        $('#author').html(`Autor: <b>${item.author}</b>`);
        $('#contact').html(`Contacto: <b>${item.contact}</b>`);
        $('#year').html(`Año: <b>${item.year}</b>`);
        $('#style').html(`Estilo: <b>${item.style}</b>`);
        $('#technique').html(`Técnica: <b>${item.technique}</b>`);
        $('#condition').html(`Condición: <b>${item.condition}</b>`);
        if (item.originalYear != -1) {
            $('#name').text(item.name + " (Réplica)");
            $('#originalYear').html(`Año original: <b>${item.originalYear}</b>`);
            $('#originalAuthor').html(`Autor original: <b>${item.originalAuthor}</b>`);
        }
        if (item.onSale) {
            $('#onSale').html(`En venta: <b>Sí</b>`);
        } else {
            $('#onSale').html(`En venta: <b>No</b>`);
        }
    } else if (type === "artStudio") {
        $('#contact').html(`Contacto: <b>${item.contact}</b>`);
        $('#address').html(`Dirección: <b>${item.address}</b>`);
        $('#year').html(`Año de inscripción: <b>${item.year}</b>`);
        $('#activities').html(`Actividades:<br>`);
        for (let i = 0; i < item.activities.length; i++) {
            let actualActivity = item.activities[i];
            $('#activities').append(`
                    <li class="ml-4">
                        <a href="#" id="${'act' + actualActivity.id}" class="text-body" style="text-decoration:none;">
                            <b>${actualActivity.name}</b>
                        </a>
                    </li>`);
            $('#act' + actualActivity.id).click(() => {
                fillItemView('activity', findItem(actualActivity.id, item.activities));
                $('#backBtn').click(() => {
                    fillItemView('artStudio', findItem(item.id, artStudios));
                });
                $('#backBtn').show();
            });
        }
    } else {
        $('#schedule').html(`Horarios: <b>${item.schedule}</b>`);
    }
}

function clearVisualizationData() {
    $('#author').html('');
    $('#contact').html('');
    $('#year').html('');
    $('#style').html('');
    $('#technique').html('');
    $('#condition').html('');
    $('#originalYear').html('');
    $('#originalAuthor').html('');
    $('#onSale').html('');
    $('#schedule').html('');
    $('#activities').html('');
    $('#address').html('');
}

function filter(grid, string) {
    const newGrid = new Grid();
    for (let i = 0; i < grid.getItems().length; i++) {
        const actualItem = grid.getItems()[i];
        if (actualItem.name.toLowerCase().includes(string.toLowerCase())) {
            newGrid.addItem(actualItem);
        }
        for (let j = 0; j < actualItem.tags.length; j++) {
            const actualTag = actualItem.tags[j];
            if (actualTag.toLowerCase().includes(string.toLowerCase())) {
                newGrid.addItem(actualItem);
                break;
            }
        }
    }
    return newGrid;
}

function toggleGenericModal(title, img, width, p) {
    $('#genericModalTitle').html(title);
    $('#genericModalBody').html(`
        <div class="mb-3 text-center">
            <img src='./assets/${img}' width=${width}>
        </div>
        <p class="m-2">${p}</p>`);
    $('#genericModal').modal('toggle');
}

function addMarker(marker, studio, key) {
    if (key === 'red') {
        map.removeLayer(redMarker);
        redMarker = marker;
    } else {
        if (!markers.includes(marker))
            markers.push(marker);
    }
    marker.bindPopup(`<div class="popup"><b>${studio.name}</b><br>${studio.address}</div>`, { maxWidth: 150 }).openPopup().addTo(map).on('click', () => {
        updateRedBorder(studio);
        addMarker(L.marker(studio.coordinates, { icon: RED_ICON }), studio, 'red');
        redMarker.openPopup();
    });
}

function removeAllMarkers() {
    markers.forEach(marker => {
        map.removeLayer(marker);
    });
    markers = [];
    map.removeLayer(redMarker);
    redMarker = {};
}

function clearCriteria(key) {
    if ($('#' + key + 'Criteria').html() != undefined) {
        $('#' + key + 'Criteria').remove();
    }
}

function updateRedBorder(studio) {
    $('#img' + redBorder.id).removeClass('border-danger border-3');
    $('#img' + redBorder.id).addClass('border-dark');
    redBorder = studio;
    $('#img' + redBorder.id).removeClass('border-dark');
    $('#img' + redBorder.id).addClass('border-danger border-3');
    $('html, body').animate({ scrollTop: ($('#img' + redBorder.id).offset().top) - ($(window).height() / 4) }, 800);
}