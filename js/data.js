let artworks = [
    {
        "id": "1",
        "name": "Figura de Muchacho",
        "author": "Spilimbergo Lino Enea",
        "description": "Retrato pintada al óleo en el año 1946. ",
        "imagePath": "./assets/artworks/art1.jpg",
        "year": 1946,
        "technique": "Óleo",
        "style": "Minimalismo",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": true,
        "contact": 1196571054,
        "tags": [
            "óleo",
            "oleo",
            "1946",
            "Spilimbergo Lino Enea",
            "minimalismo",
            "original",
            "si disponible"
        ]
    },
    {
        "id": "2",
        "name": "Figura sentada con flores",
        "author": "Cogorno Santiago",
        "description": "Figura pintada al óleo sobre tela en el año 1987.",
        "imagePath": "./assets/artworks/art2.jpg",
        "year": 1987,
        "technique": "Óleo",
        "style": "Arte cinético",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": false,
        "contact": 1175247052,
        "tags": [
            "óleo",
            "oleo",
            "1987",
            "Cogorno Santiago",
            "arte cinético",
            "arte cinetico",
            "original",
            "no disponible"
        ]
    },
    {
        "id": "3",
        "name": "Paisaje",
        "author": "Marziani Hugo",
        "description": "Paisaje pintado con acrílico sobre tela en el año 1978.",
        "imagePath": "./assets/artworks/art3.jpg",
        "year": 1978,
        "technique": "Acrílico",
        "style": "Informalismo",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": true,
        "contact": 1162047902,
        "tags": [
            "acrílico",
            "acrilico",
            "1978",
            "Marziani Hugo",
            "informalismo",
            "original",
            "si disponible"
        ]
    },
    {
        "id": "4",
        "name": "Composición",
        "author": "Grilo Sarah",
        "description": "Pintura con estilo abstracto y con abstracciones geométricas realizada en el año 1960.",
        "imagePath": "./assets/artworks/art4.jpg",
        "year": 1960,
        "technique": "Abstracto",
        "style": "Constructivismo",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": false,
        "contact": 1187042070,
        "tags": [
            "abstracto",
            "1960",
            "Grilo Sarah",
            "constructivismo",
            "original",
            "no disponible"
        ]
    },
    {
        "id": "5",
        "name": "Paisaje de Tucumán",
        "author": "Linares Joaquín Ezequiel",
        "description": "Paisaje pintado con óleo sobre cartón en el año 1966. ",
        "imagePath": "./assets/artworks/art5.jpg",
        "year": 1966,
        "technique": "Óleo",
        "style": "Dadaísmo",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": true,
        "contact": 1150707075,
        "tags": [
            "óleo",
            "oleo",
            "1966",
            "Linares Joaquín Ezequiel",
            "dadaísmo",
            "dadaismo",
            "original",
            "si disponible"
        ]
    },
    {
        "id": "6",
        "name": "Florero sobre paño blanco",
        "author": "Menghi Jose Luis",
        "description": "Florero pintado con óleo sobre hardboard en el año 1934.",
        "imagePath": "./assets/artworks/art6.jpg",
        "year": 1934,
        "technique": "Óleo",
        "style": "Arte cinético",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": false,
        "contact": 1189507078,
        "tags": [
            "óleo",
            "oleo",
            "1934",
            "Menghi Jose Luis",
            "arte cinético",
            "arte cinetico",
            "original",
            "no disponible"
        ]
    },
    {
        "id": "7",
        "name": "La montaña",
        "author": "Nojechowicz Noe",
        "description": "Paisaje pintado en óleo sobre tela en el año 1974.",
        "imagePath": "./assets/artworks/art7.jpg",
        "year": 1974,
        "technique": "Óleo",
        "style": "Arte cinético",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": true,
        "contact": 1169372015,
        "tags": [
            "óleo",
            "oleo",
            "1974",
            "Nojechowicz Noe",
            "arte cinético",
            "arte cinetico",
            "original",
            "si disponible"
        ]
    },
    {
        "id": "8",
        "name": "Crónica de respetables",
        "author": "Oblar Pablo",
        "description": "Grabado con serigrafía en el año 1977.",
        "imagePath": "./assets/artworks/art8.jpg",
        "year": 1977,
        "technique": "Serigrafía",
        "style": "Simbolismo",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": false,
        "contact": 1169734570,
        "tags": [
            "serigrafía",
            "1977",
            "Oblar Pablo",
            "simbolismo",
            "original",
            "no disponible"
        ]
    },
    {
        "id": "9",
        "name": "Pintura",
        "author": "Pino Felipe",
        "description": "Pintura realizada en acrílico sobre tela en 1986. ",
        "imagePath": "./assets/artworks/art9.jpg",
        "year": 1986,
        "technique": "Acrílico",
        "style": "Simbolismo",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": true,
        "contact": 1164975004,
        "tags": [
            "acrílico",
            "acrilico",
            "1986",
            "Pino Felipe",
            "simbolismo",
            "original",
            "si disponible"
        ]
    },
    {
        "id": "10",
        "name": "Cabeza",
        "author": "Seoane Luis",
        "description": "Pintura realizada óleo sobre lienzo en el año 1958.",
        "imagePath": "./assets/artworks/art10.jpg",
        "year": 1958,
        "technique": "Óleo",
        "style": "Arte cinético",
        "condition": "Original",
        "originalYear": -1,
        "originalAuthor": '',
        "onSale": false,
        "contact": 1164975034,
        "tags": [
            "óleo",
            "oleo",
            "1958",
            "Seoane Luis",
            "arte cinético",
            "arte cinetico",
            "original",
            "no disponible"
        ]
    },
    {
        "id": "11",
        "name": "Midi en hiver III",
        "author": "Lucas Solano",
        "description": "Pintura realizada óleo sobre lienzo en el año 1964.",
        "imagePath": "./assets/artworks/art11.png",
        "year": 2016,
        "technique": "Óleo",
        "style": "Arte cinético",
        "condition": "Réplica",
        "originalYear": 1964,
        "originalAuthor": "Emilio Pettoruti",
        "onSale": true,
        "contact": 1169524125,
        "tags": ["óleo", "oleo", "2016", "1964", "Lucas Solano", "Emilio Pettoruti", "arte cinético", "arte cinetico", "replica", "réplica", "si disponible"]
    },
    {
        "id": "12",
        "name": "Le cirque bleu",
        "author": "Daniel Lauriente",
        "description": "El personaje principal es una trapecista, inmersa en un mundo fantástico de música y circo, este último uno de los temas pictóricos favoritos del artista, quien dijo: 'Para mí el circo es un espectáculo mágico que aparece y desaparece como un mundo'. Pintado hacia sus últimos años, demuestra un gran sentido del movimiento y ritmo.",
        "imagePath": "./assets/artworks/art12.png",
        "year": 2018,
        "technique": "Óleo",
        "style": "Modernismo",
        "condition": "Réplica",
        "originalYear": 1950,
        "originalAuthor": "Marc Chagall",
        "onSale": true,
        "contact": 1152417452,
        "tags": ["óleo", "oleo", "2018", "1950", "Daniel Lauriente", "Marc Chagall", "modernismo", "replica", "réplica", "si disponible"]
    },
    {
        "id": "13",
        "name": "Desnudo con gato",
        "author": "Lorena Pintos",
        "description": "Obra en la cual se retrata a la poeta Anna Ajmátova. Durante varios años no se supo quién era la mujer retratada por el pintor. Sin embargo, en el 2008 el rostro de la vate rusa fue reconocido por los investigadores Jorge Bustamante e Irina Ostroúmova, mientras visitaban el museo mexicano Soumaya. Dicha obra pertenecía a la colección privada del gran amigo del pintor, Paul Alexandre, hasta la muerte de este.",
        "imagePath": "./assets/artworks/art13.png",
        "year": 2010,
        "technique": "Carboncillo y lápiz",
        "style": "Impresionismo",
        "condition": "Réplica",
        "originalYear": 1910,
        "originalAuthor": "Amedeo Modigliani",
        "onSale": true,
        "contact": 1167548721,
        "tags": ["carboncillo y lápiz sobre papel", "Carboncillo y lapiz sobre papel", "2010", "1910", "Lorena Pintos", "Amedeo Modigliani", "impresionismo", "replica", "réplica", "si disponible"]
    }
];

let artStudios = [
    {
        "id": "1",
        "name": "Miriam Diaz",
        "sponsor": false,
        "address": "Gral. las Heras 1061, Muñiz, Provincia de Buenos Aires",
        "coordinates": [-34.546297, -58.704107],
        "city": "Muñiz",
        "description": "Escuela de arte en Muñiz.",
        "imagePath": "./assets/artstudios/as1.jpg",
        "year": 2020,
        "contact": 1146641615,
        "activities": [
            {
                "id": "1",
                "name": "Taller de platos de sitio",
                "description": "Hermosos platos de sitio para tu mesa!! Para regalar o una mesa de Navidad!! Hacemos 3 platos iguales o diferentes distintas técnicas!!",
                "schedule": "Viernes de 16 a 19 hs o sábado de 14:30 a 17:30 hs",
                "imagePath": "./assets/activities/a1.jpg"
            },
            {
                "id": "2",
                "name": "Taller de pintura",
                "description": "Aprende a pintar sobre cualquier superficie.",
                "schedule": "Lunes a viernes de 13 a 16 hs",
                "imagePath": "./assets/activities/a2.jpg"
            }
        ],
        "tags": ["muñiz", "taller de platos de sitio", "taller de pintura", "2020"]
    },
    {
        "id": "2",
        "name": "Academia del Dibujo Comic y Manga",
        "sponsor": true,
        "address": "Italia 531, San Miguel, Provincia de Buenos Aires",
        "coordinates": [-34.539278, -58.702863],
        "city": "San Miguel",
        "description": "Escuela De Arte en San Miguel",
        "imagePath": "./assets/artstudios/as2.jpg",
        "year": 2019,
        "contact": 1122619730,
        "activities": [
            {
                "id": "3",
                "name": "Curso online de comic y manga",
                "description": "ANOTESE YA Y ABONE CON DESCUENTO!!!",
                "schedule": "Lunes y miércoles de 20 a 22 hs",
                "imagePath": "./assets/activities/a3.jpg"
            },
            {
                "id": "4",
                "name": "Taller de dibujo",
                "description": "Dibujo e ilustración Realista en San Miguel!",
                "schedule": "Martes y jueves de 13 a 16 hs",
                "imagePath": "./assets/activities/a4.jpg"
            },
            {
                "id": "5",
                "name": "Taller de arte y dibujo para chicos",
                "description": "Taller de Arte y Dibujo para chicos! arte para niños y niñas, clases de dibujo en San Miguel! Clases para chicos! Actividades para chicos, dibujo, arte, etc",
                "schedule": "Miércoles y viernes de 15 a 18 hs",
                "imagePath": "./assets/activities/a5.jpg"
            }
        ],
        "tags": ["san miguel", "curso online de comic y manga", "taller de dibujo", "taller de arte", "taller de arte y dibujo para chicos", "2019"]
    },
    {
        "id": "3",
        "name": "Escuela Municipal de Artes Visuales",
        "sponsor": false,
        "address": "Av. Pres. Juan Domingo Perón 1102, San Miguel, Provincia de Buenos Aires",
        "coordinates": [-34.546068, -58.706527],
        "city": "San Miguel",
        "description": "Escuela propiedad de la Municipalidad de San Miguel",
        "imagePath": "./assets/artstudios/as3.jpg",
        "year": 2015,
        "contact": 1160917100,
        "activities": [
            {
                "id": "6",
                "name": "Taller de Comedia Musical",
                "description": "Te animás a ser parte de un gran show? Taller de comedia musical. Si te gusta bailar, cantar, actuar y sos mayor de 18 años te esperamos.",
                "schedule": "Martes de 19:00 a 20:30hs",
                "imagePath": "./assets/activities/a6.jpg"
            }
        ],
        "tags": ["san miguel", "taller de comedia musical", "2015"]
    },
    {
        "id": "4",
        "name": "Escuela de Arte y Psicodrama",
        "sponsor": false,
        "address": "Conesa 970, San Miguel, Provincia de Buenos Aires",
        "coordinates": [-34.546014, -58.702727],
        "city": "San Miguel",
        "description": "Escuela",
        "imagePath": "./assets/artstudios/as4.jpg",
        "year": 2018,
        "contact": 1146630485,
        "activities": [
            {
                "id": "7",
                "name": "Formación en Psicodrama",
                "description": "Modalidad virtual - Nivel introductorio - Nivel superior - Inteligencia emocional - Entrenamiento en teatro espontáneo",
                "schedule": "Martes y viernes de 16:00 a 18:00hs",
                "imagePath": "./assets/activities/a7.png"
            },
            {
                "id": "8",
                "name": "Del Aislamiento al Intercambio de Sentires y Emociones",
                "description": "Por sala ZOOM",
                "schedule": "Lunes y miércoles de 20:00 a 22:00hs",
                "imagePath": "./assets/activities/a8.png"
            }
        ],
        "tags": ["san miguel", "formación en psicodrama", "formacion en psicodrama", "daise", "del aislamiento al intercambio de sentires y emociones", "2018"]
    },
    {
        "id": "5",
        "name": "Escuela Moda Arte Diseño",
        "sponsor": true,
        "address": "España 844, San Miguel, Provincia de Buenos Aires",
        "coordinates": [-34.537160, -58.712074],
        "city": "San Miguel",
        "description": "Escuela de arte",
        "imagePath": "./assets/artstudios/as5.jpg",
        "year": 2018,
        "contact": 1146677313,
        "activities": [
            {
                "id": "9",
                "name": "Taller de Arte, dibujo y pintura",
                "description": `Dibujo y pintura artística en lápiz, carbonilla, pasteles, acrílico, óleo, acuarela, técnicas mixtas. Especialidad técnica con espátula.
                    <br>Pintura decorativa sobre madera, vidrio, telas,bizcocho de cerámica, otros soportes. Consultar por otras técnicas.
                    <br>Para principiantes o avanzados.<br>
                    <br><b>Comienzo</b><br>
                    Todos los meses<br>
                    <br><b>Certificado</b><br>
                    Expedido por la Escuela M.A.D.`,
                "schedule": "Martes y jueves de 14:00 a 18:00hs",
                "imagePath": "./assets/activities/a9.png"
            },
            {
                "id": "10",
                "name": "Tejido Crochet y Dos Agujas",
                "description": `<b>El tejido croché</b> es una técnica para tejer labores con hilo o lana que utiliza una aguja corta y específica, «aguja de croché» de metal, plástico o madera. Esta labor consiste en pasar un anillo de hilo por encima de otro, aunque a diferencia de éste,  se trabaja solamente con uno de los anillos cada vez. Se pueden realizar múltiples tejidos como colchas, puntillas, centros de mesa, prendas de  vestir, objetos de decoración, accesorios de moda, etc.<br>
                    <br><b>El tejido a dos agujas</b> es un método milenario que se utiliza para tejer con lana, aunque  pueden utilizarse también otros materiales como el hilo de algodón, cintas, totoras, etc.  Hacer punto consiste en dar una serie de lazadas (llamadas puntos), unidas entre ellas de  forma que constituyan una malla.  Para el punto se utilizan, habitualmente, dos largas agujas con las que se maneja el hilo de lana  para dar forma a la lazada. El grosor de las agujas determina en gran medida el tamaño del  punto y, con él, la tupidez de la malla o tejido resultante.<br>
                    <br><b>Comienzo</b><br>
                    Todos los meses<br>
                    <br><b>Cursada</b><br>
                    1 vez por semana – 2 horas<br>
                    <br><b>Certificado</b><br>
                    Expedido por la Escuela M.A.D.`,
                "schedule": "Lunes de 16:00 a 18:00hs",
                "imagePath": "./assets/activities/a10.png"
            }
        ],
        "tags": ["san miguel", "taller de arte, dibujo y pintura", "taller de arte", "taller de dibujo", "taller de pintura", "tejido crochet y dos agujas", "2018"]
    },
    {
        "id": "6",
        "name": "Pentagrama Estudio",
        "sponsor": false,
        "address": " Av. Primera Junta 1101, San Miguel, Provincia de Buenos Aires",
        "coordinates": [-34.533070, -58.723579],
        "city": "San Miguel",
        "description": "Escuela de arte en San Miguel",
        "imagePath": "./assets/artstudios/as6.png",
        "year": 2017,
        "contact": 1154329559,
        "activities": [
            {
                "id": "11",
                "name": "Iniciación a comedia musical",
                "description": "De 4 a 6 años",
                "schedule": "Martes y jueves de 14:00 a 18:00hs",
                "imagePath": "./assets/activities/a11.jpg"
            }
        ],
        "tags": ["san miguel", "iniciación a comedia musical", "iniciacion a comedia musical", "2017"]
    }
];